<?php
include_once('authData.php');
include_once('settings.php');
include_once('functions.php');

auth();
echo "\r\n";
update_storage(TRUE);
echo "\r\n";
update_info(TRUE);
echo "\r\n";
get_companies();
get_military_companies_inventory();
initialize_stats();

$fail = 0;

while (eat() && $fail < 3 && is_equipped())
{
	//if we have energy for fight
	if ($energy > 4)
	{
		//and we health is full
		if ($health === $max_health)
		{
			//and we have no traumas
			if ($injured['light'] == 0 && $injured['medium'] == 0 && $injured['height'] == 0)
			{
				//then fight
				echo 'fight set to '.$last_gang.'/'.$last_rank."\r\n";
				
				curl_setopt($ch, CURLOPT_URL, $domain.'/military/pve/gangs/'.$last_band.'/'.$last_gang.'/ranks/'.$last_rank.'/fight');
				curl_setopt($ch, CURLOPT_POST, TRUE);

				$result = json_decode(curl_exec($ch), true);
				if ($result['userWin'])
				{
					$fail = 0;
					echo 'you WIN the fight'."\r\n";
					echo 'exp: '.$result['result']['experienceEarned']."\r\n";
					echo 'health: '.$result['result']['healthLoss']."\r\n";
					write_stats('win', $result['result']['causedDamage'], $result['info']['causedDamage'], $result['info']['nextRankCausedDamage'], $result['result']['vdEarned']);
				}
				else
				{
					$fail ++;
					if ($fail === 1)
					{
						echo 'FIRST FAIL'."\r\n";
					}
					else
					{
						echo 'SECOND FAIL'."\r\n";
					}
					echo 'you LOSE the fight'."\r\n";
					echo 'exp: '.$result['result']['experienceEarned']."\r\n";
					write_stats('lose', $result['result']['causedDamage'], $result['info']['causedDamage'], $result['info']['nextRankCausedDamage']);
				}
				if (count($result['result']['traumas']))
				{
					echo 'you are injured'."\r\n";
				}
				foreach ($weapon as $key => $value) 
				{
					if ($value['equipped'] === 1)
					{
						if ($value['itemStrength'] === 1)
						{
							echo 'weapon #'.$value['id'].' was spent in '.$value['slot']."\r\n";
						}
					}
				}
				foreach ($armor as $key => $value) 
				{
					if ($value['equipped'] === 1)
					{
						if ($value['itemStrength'] === 1)
						{
							echo 'armor #'.$value['id'].' was spent in '.$value['slot']."\r\n";
						}
					}
				}
				$used_ammo = 0;
				//var_dump($result['result']['itemsLost']);
				
				foreach ($ammunition as $key => $ammo) 
				{
					foreach ($result['result']['itemsLost'] as $value) 
					{
						if ($ammo['id'] === $value['id'])
						{
							$used_ammo += $value['quantity'];
							if ($ammunition[$key]['quantity'] < 1)
							{
								echo 'ammunition #'.$value['id'].' was spent in '.$ammo['slot']."\r\n";
							}
						}
					}
				}
				echo 'used '.$used_ammo.' ammo'."\r\n";
				update_storage();
				echo '-----------------------------------'."\r\n";
			}
			else
			{
				heal_injury();
			}
			update_info();
			echo 'energy: '.$energy."\r\n";
			echo 'money: '.number_format($vd_balance, 2, '.', ' ')."\r\n";
		}
		else
		{
			heal();
		}
	}
	else
	{
		echo 'energy check fail ('.$energy.')'."\r\n";
		sleep (500);
		update_info();	
	}
}
die();
if ($health < 1)
{
	heal();
}
while (TRUE)
{

	//if we have energy for training
	if ($energy > 9)
	{
		//and we have no traumas
		if ($injured['light'] == 0 && $injured['medium'] == 0 && $injured['height'] == 0)
		{
			//let's training
			get_coach_license_id();

			curl_setopt($ch, CURLOPT_URL, $domain.'/sport_activities/use_program.json?os=android&v=1.05');
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "data%5BCoachProgram%5D%5Blicense_id%5D=$CoachProgramLicenseId&data%5BCoachProgram%5D%5Bid%5D=$CoachProgramId&data%5BSportActivity%5D%5Bsport_skill_type_id%5D=3&data%5BSportActivity%5D%5Benergy%5D=10");

			$result = json_decode(curl_exec($ch), true);
			if (isset($result['setFlash'][0]['class']) && $result['setFlash'][0]['class'] === 'flash_success')
			{
				echo 'training successful'."\r\n";
			}
			else
			{
				die ('Something went wrong (1):'."\r\n".print_r($result, true)."\r\n");
			}

		}
		else
		{
			heal_injury();
		}
		update_info();
	}
	else
	{
		echo 'energy check fail ('.$energy.')'."\r\n";
		sleep (100);
		update_info();
	}
}


curl_close($ch);

