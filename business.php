<?php
	include_once('authData.php');
	include_once('settings.php');
	include_once('functions.php');

	auth();

	echo "\r\n";
	get_companies();
	update_companies_balance();
	update_info(TRUE);
	update_storage(TRUE);
	eat();

	echo "Transfer of money to companies...\r\n\r\n";

	echo "Companies balance:\r\n";
	foreach ($companies as $id => $company) 
	{
		echo str_pad($company['id'], 6) . 
			 str_pad($company['type_name'], 30) .
			 str_pad($company['name'], 28)."\t".
			 str_pad($company['foreign_employees_limit'], 2, ' ', STR_PAD_LEFT)."\t$".
			 str_pad(number_format($company['vd_balance'], 2, '.', ' '), 12, ' ', STR_PAD_LEFT)."\r\n";
	}

	$all_money = get_summ_vd_balance($companies) + $vd_balance;
	$workers = get_summ_foreign_employees($companies);
	$full_hours = floor($all_money / ($workers * $PP_per_hour * $Salary_per_PP));
	if ($full_hours > 48)
	{
		$full_hours = 48;
	}
	$money_per_worker = $full_hours * $PP_per_hour * $Salary_per_PP;

	$delta = 0;
	foreach ($companies as $id => $company)
	{
		if ($company['vd_balance'] > $company['foreign_employees_limit'] * $money_per_worker)
		{
			$delta += $company['vd_balance'];
			$workers -= $company['foreign_employees_limit'];
		}
	}

	if ($delta > 0)
	{
		//recalculate
		$all_money = get_summ_vd_balance($companies) + $vd_balance - $delta;
		$full_hours = floor($all_money / ($workers * $PP_per_hour * $Salary_per_PP));
		if ($full_hours > 48)
		{
			$full_hours = 48;
		}
		$money_per_worker = $full_hours * $PP_per_hour * $Salary_per_PP;
	}

	if ($full_hours > 1)
	{
		echo "Money transfer for $full_hours hour(s)\r\n\r\n";
	}
	else
	{
		echo "No money for even one hour\r\n";
	}

	foreach ($companies as $id => $company)
	{
		$need_money = $company['foreign_employees_limit'] * $money_per_worker;
		if ($company['vd_balance'] < $need_money)
		{
			$transfer = $need_money - $company['vd_balance'];
			echo 'Transfering $' . $transfer . ' to ' . $company['name'] . ' ... ';
			if (transfer_vd_to_company($id, $transfer))
			{
				$companies[$id]['vd_balance'] = $need_money;
				echo 'COMPLETE' . "\r\n";
			}
			else
			{
				echo 'FAILED' . "\r\n";
			}
		}
	}

	echo "\r\nCoal ==> Proccessing\r\n";
	move_items_from_to(
							19, 
							array(
								8581, 
								1800, 
								6271,
								14709
							), 
							array(
								14571 => 0.10,
							   	1737  => 0.20, 
							   	8546  => 0.20, 
							   	4027  => 0.20,
							   	12943 => 0.20,
							   	14572 => 0.10
						   	)
					   );
	echo "\r\nOre ==> Proccessing\r\n";
	move_items_from_to(
							419, 
							array(
								2119,
								2804,
								3206,
								8208,
								12226,
								8670,
								9172,
								9528,
								9468,
								1809
							), 
							array(
							   	4027  => 0.666666,
							   	14571 => 0.333334
						   	)
					   );
	echo "\r\nCotton ==> Proccessing\r\n";
	move_items_from_to(
							420, 
							array(
								5220,
								14710
							), 
							array(
							   	8546 => 1
						   	)
					   );
	echo "\r\nSkin ==> Proccessing\r\n";
	move_items_from_to(
							422, 
							array(
								5396
							), 
							array(
							   	14572 => 1
						   	)
					   );
	echo "\r\nGems ==> Proccessing\r\n";
	move_items_from_to(
							424, 
							array(
								10341
							), 
							array(
							   	1737 => 1
						   	)
					   );
	echo "\r\nPrecious ore ==> Proccessing\r\n";
	move_items_from_to(
							418, 
							array(
								10791
							), 
							array(
							   	1737 => 1
						   	)
					   );
	echo "\r\nSawdust ==> Proccessing\r\n";
	move_items_from_to(
							423, 
							array(
								3843,
								14708
							), 
							array(
							   	12943 => 1
						   	)
					   );
	echo "\r\nAluminium ==> MP\r\n";
	move_items_from_to(
							321, 
							array(
								14571,
								4027 
							), 
							array(
							   	1813  => 0.36961, //ak
								8174  => 0.36961, //ak
								14573 => 0.03392, //helm
								14574 => 0.04472, //boots
								14575 => 0.05622, //OTV
								5455  => 0.12593  //pants
						   	)
					   );
	echo "\r\nChipboard 10 mm ==> MP\r\n";
	move_items_from_to(
							333, 
							array(
								12943 
							), 
							array(
							   	14573 => 0.28412, //helm
								14574 => 0.23407, //boots
								14575 => 0.19618, //OTV
								5455  => 0.28563  //pants
						   	)
					   );
	echo "\r\nCopper ==> MP\r\n";
	move_items_from_to(
							325, 
							array(
								1737
							), 
							array(
							   	1813  => 0.5, //ak
								8174  => 0.5 //ak
						   	)
					   );
	echo "\r\nSapphire ==> MP\r\n";
	move_items_from_to(
							345, 
							array(
								1737
							), 
							array(
							   	1813  => 0.5, //ak
								8174  => 0.5 //ak
						   	)
					   );
	echo "\r\nFine fabric ==> MP\r\n";
	move_items_from_to(
							341, 
							array(
								8546
							), 
							array(
							   	14573 => 0.16365, //helm
								14574 => 0.19415, //boots
								14575 => 0.21696, //OTV
								5455  => 0.42524  //pants
						   	)
					   );
	echo "\r\nLeather ==> MP\r\n";
	move_items_from_to(
							337, 
							array(
								14572
							), 
							array(
							   	14573 => 0.17853, //helm
								14574 => 0.17650, //boots
								14575 => 0.14793, //OTV
								5455  => 0.49704  //pants
						   	)
					   );

	update_info(TRUE);