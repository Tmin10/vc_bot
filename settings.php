<?php
	$mysqli = new mysqli($mysql_domain, $mysql_login, $mysql_password, $mysql_db);
	if (mysqli_connect_errno())
	{
		printf("Connect failed: %s\n", mysqli_connect_error());
		exit();
	}

	$mysqli->query("SET NAMES utf8");

	$last_band = 13;
	$last_gang = 5;
	$last_rank = 2;


	$ch = null;
	
	$vd_balance = 0;
	$vg_balance = 0;

	$energy = 0;
	$max_energy = 0;
	$health = 0;
	$max_health = 0;
	$userId = 0;
	$CoachProgramId = 0;
	$CoachProgramLicenseId = 0;
	$free_slots = 0;
	$delta_recovery_energy = 0;
	$min_ate_end_time = 111111111111;

	$injured = array();
	$ammunition = array();
	$weapon = array();
	$armor = array();
	$food = array();
	$eatEffects = array();
	$companies = array();

	$domain = 'https://api3.vircities.com';
	$version = '?os=android&v=1.50';

	$id_array = array(
				//armor
				'helmet'			=> 234,
				'OTV_body_armor'	=> 260,
				'tactical_pants'	=> 292,
				'membrane_boots'	=> 319,

				//weapon & ammo
				'ammo_762mm' 		=> 143,
				'AK74_riffle' 		=> 369,
				
				//food
				'fish'				=> 7,
				'milk'				=> 25,
				'grain'				=> 28,
				'meat_bread'		=> 381,
				'fish_pie'			=> 382,
				'stew'				=> 383,
				'fishers_pizza'		=> 384,
				'vegetables'		=> 421,
				'meat'				=> 590

				);

	$PP_per_hour = 25;
	$Salary_per_PP = 100;