<?php
include_once('authData.php');
include_once('settings.php');
include_once('functions.php');

while (true)
{
	
	auth();
	echo "\r\n";
	update_storage(TRUE);
	echo "\r\n";
	update_info(TRUE);
	echo "\r\n";
	eat();
	update_info();
	$seconds = $min_ate_end_time - time();
	if ($seconds <= 90)
	{
		$seconds = 90;
	}

	if ($injured['light'] !== 0 || $injured['medium'] !== 0 || $injured['height'] !== 0)
	{
		heal_injury();
	}
	while ($seconds > 0)
	{
		echo "\r".$seconds;
		sleep(1);
		$seconds --;
		echo "\r     ";
	}
}