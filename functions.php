<?php
function reverse($text, $i = 7)
{
	//1,2,3,4,7,8,9
	return chr(27)."[".$i."m".$text.chr(27)."[0m";
}

function auth()
{
	global $ch, $login, $password, $userId, $domain, $version;

	$ch = curl_init($domain."/mobile/1.10/list.js");

	//curl_setopt($ch, CURLOPT_HEADER, TRUE);
	curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_TIMEOUT, 50);
	curl_setopt($ch, CURLOPT_ENCODING, "gzip");
	curl_setopt($ch, CURLOPT_COOKIEFILE, "cookies.txt");
	curl_setopt($ch, CURLOPT_POST, TRUE);
	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Linux; Android 4.4.2; NoxW Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");
	curl_setopt($ch, CURLOPT_HTTPHEADER, array (
												'Origin: file://',
												'Content-type: application/x-www-form-urlencoded',
												'X-Requested-With: com.vircities.vc_mobile'
											    ));
	curl_exec($ch);

	curl_setopt($ch, CURLOPT_POST, TRUE);
	curl_setopt($ch, CURLOPT_URL, $domain."/users/app_auth.json".$version);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "data%5BUser%5D%5Busername%5D=$login&data%5BUser%5D%5Bpassword%5D=$password");

	$raw = curl_exec($ch);
	$result = json_decode($raw, true);
	if (isset ($result['userId']))
	{
		//echo 'Authorization complete:'."\r\n".'userId: '.$result['userId']."\r\n";
		//echo 'currentLevel: '.$result['currentLevel']."\r\n";
		$userId = 20157;//$result['userId'];
		echo 'userId: '.$result['userId']."\r\n";
	}
	else
	{
		var_dump(curl_error($ch));
		die ('Something went wrong (3):'."\r\n".print_r($raw, true)."\r\n");
	}
}

function update_info($verbose = FALSE)
{
	global $energy, $max_energy, $health, $max_health, $ch, $injured, $domain, $vd_balance, $vg_balance, $version, 
		   $delta_recovery_energy, $eatEffects, $min_ate_end_time;

	$eatEffects = array();
	$min_ate_end_time = 111111111111;

	curl_setopt($ch, CURLOPT_URL, $domain."/users/short_infos.json".$version);
	curl_setopt($ch, CURLOPT_HTTPGET, TRUE);

	$result = json_decode(curl_exec($ch), true);
	if (isset ($result['user']))
	{
		if ($verbose === TRUE)
		{
			echo 'xp: '.$result['user']['UserLevel']['xp'].'/'.$result['user']['UserLevel']['nextLevelExperience']."\r\n";
			echo 'balance: '.number_format($result['user']['User']['vd_balance'], 2, '.', ' ').'/'.$result['user']['User']['vg_balance']."\r\n";
			echo 'energy: '.$result['user']['User']['energy'].'/'.$result['user']['User']['max_energy']."\r\n";
			echo 'health: '.$result['user']['User']['health'].'/'.$result['user']['User']['max_health']."\r\n";
			echo 'delta_recovery_energy: '.$result['user']['User']['delta_recovery_energy']."\r\n";
			echo 'injured: '.$result['injured']['light'].'/'.$result['injured']['medium'].'/'.$result['injured']['height']."\r\n\r\n";
			
			/*
			curl_setopt($ch, CURLOPT_URL, $domain."/military/pve/gangs");
			$result_gang = json_decode(curl_exec($ch), true);
			foreach ($result_gang['progress'] as $progress)
			{
				echo '#'.$progress['complexity'].': '.str_pad((($progress['wins'][0]==0)?reverse(' 0'):$progress['wins'][0]), 4, ' ', STR_PAD_LEFT).'/'.$progress['winsUnlock'][1].', '.
						str_pad((($progress['wins'][1]==0)?reverse('   0'):$progress['wins'][1]), 4, ' ', STR_PAD_LEFT).'/'.$progress['winsUnlock'][2].', '.
						str_pad((($progress['wins'][2]==0)?reverse('   0'):$progress['wins'][2]), 4, ' ', STR_PAD_LEFT).'/'.$progress['winsUnlock'][3].', '.
						str_pad((($progress['wins'][3]==0)?reverse('   0'):$progress['wins'][3]), 4, ' ', STR_PAD_LEFT).'/'.$progress['winsUnlock'][4]."\r\n";
			}
			echo "\r\n";
			*/
		}

		$eatEffects = $result['eatEffects'];

		foreach ($eatEffects as $effect) 
		{
			if ($effect['end'] < $min_ate_end_time)
			{
				$min_ate_end_time = $effect['end'];
			}
		}

		$energy = (int) $result['user']['User']['energy'];
		$max_energy = (int) $result['user']['User']['max_energy'];
		$health = (int) $result['user']['User']['health'];
		$max_health = (int) $result['user']['User']['max_health'];
		$injured = $result['injured'];
		$vd_balance = (float) $result['user']['User']['vd_balance'];
		$vg_balance = (float) $result['user']['User']['vg_balance'];
		$delta_recovery_energy = (float) $result['user']['User']['delta_recovery_energy'];
			
	}
	else
	{
		die ('Something went wrong (2):'."\r\n".print_r($result, true)."\r\n");
	}
}

function update_storage($verbose = FALSE)
{
	global $ch, $ammunition, $weapon, $domain, $version, $armor, $id_array, $free_slots, $food;

	$ammunition = array();
	$weapon = array();
	$armor = array();
	$food = array();


	curl_setopt($ch, CURLOPT_URL, $domain."/user_items/storage.json".$version);
	curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
	$result = json_decode(curl_exec($ch), true);
	//var_dump($result);
	if (isset ($result['storage']))
	{
		$equipped_count = 0;
		foreach($result['storage']['itemsArray'] as $key => $value)
		{
			if ($value['UserItem']['equipped'] === 1)
			{
				$equipped_count ++;
			}
			if ($value['UserItem']['item_type_id'] === $id_array['ammo_762mm'])
			{
				$ammunition[] = array('name'		=> $result['storage']['itemTypes'][$value['UserItem']['item_type_id']]['name'], 
									  'equipped'	=> $value['UserItem']['equipped'], 
									  'quantity'	=> $value['UserItem']['quantity'],
									  'slot'		=> $value['UserItem']['equipped_slot'],
									  'id'			=> $value['UserItem']['id'],
									  'item_type'	=> $value['UserItem']['item_type_id']);
			}
			if ($value['UserItem']['item_type_id'] === $id_array['AK74_riffle'])
			{
				$weapon_id = $value['UserItem']['id'];
				curl_setopt($ch, CURLOPT_URL, $domain.'/user_items/get_info/'.$weapon_id.'.json'.$version);
				curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
				$weapon_result = json_decode(curl_exec($ch), true);
				if (isset ($weapon_result['itemStrength']))
				{
					//for stupid non-english name
					$weapon[] = array('name' 			=> 'AK-74 riffle',//$value['ItemType']['name_en'], 
									  'equipped' 		=> $value['UserItem']['equipped'], 
									  'strengthStart'	=> $weapon_result['strengthStart'], 
									  'itemStrength' 	=> $weapon_result['itemStrength'],
									  'slot'			=> $value['UserItem']['equipped_slot'],
									  'id'				=> $weapon_id,
									  'item_type'		=> $value['UserItem']['item_type_id']);
				}
				else
				{
					die ('Something went wrong (4):'."\r\n".print_r($weapon_result, true)."\r\n");
				}
			}
			if ($value['UserItem']['item_type_id'] === $id_array['tactical_pants'] || 
				$value['UserItem']['item_type_id'] === $id_array['membrane_boots'] ||
				$value['UserItem']['item_type_id'] === $id_array['OTV_body_armor'] ||
				$value['UserItem']['item_type_id'] === $id_array['helmet'])
			{
				$armor_id = $value['UserItem']['id'];
				curl_setopt($ch, CURLOPT_URL, $domain.'/user_items/get_info/'.$armor_id.'.json'.$version);
				curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
				$armor_result = json_decode(curl_exec($ch), true);	
				if (isset ($armor_result['itemStrength']))
				{
					$armor[] = array('name' 			=> $result['storage']['itemTypes'][$value['UserItem']['item_type_id']]['name'], 
									 'equipped' 		=> $value['UserItem']['equipped'], 
									 'strengthStart'	=> $armor_result['strengthStart'], 
									 'itemStrength' 	=> $armor_result['itemStrength'],
									 'slot'				=> $value['UserItem']['equipped_slot'],
									 'id'				=> $armor_id,
									 'item_type'		=> $value['UserItem']['item_type_id']);
				}
				else
				{
					die ('Something went wrong (41):'."\r\n".print_r($weapon_result, true)."\r\n");
				}
			}

			if ($value['UserItem']['item_type_id'] === $id_array['fish_pie'] || 
				$value['UserItem']['item_type_id'] === $id_array['meat_bread'] ||
				$value['UserItem']['item_type_id'] === $id_array['fishers_pizza'] ||
				$value['UserItem']['item_type_id'] === $id_array['stew'] ||
				$value['UserItem']['item_type_id'] === $id_array['grain'] ||
				$value['UserItem']['item_type_id'] === $id_array['fish'] ||
				$value['UserItem']['item_type_id'] === $id_array['milk'] ||
				$value['UserItem']['item_type_id'] === $id_array['vegetables'] ||
				$value['UserItem']['item_type_id'] === $id_array['meat']
				)
			{
				$food[] = array('name' 			=> $result['storage']['itemTypes'][$value['UserItem']['item_type_id']]['name'],
								'id'			=> $value['UserItem']['id'],
								'item_type'		=> $value['UserItem']['item_type_id'],
								'quantity'		=> $value['UserItem']['quantity']);
			}

		}

		$free_slots = $result['storage']['inventorySize'] - count($result['storage']['itemsArray']) + $equipped_count;

		if ($verbose === TRUE)
		{
			echo 'storage: ' . (count($result['storage']['itemsArray']) - $equipped_count) . '/' . $result['storage']['inventorySize'] . "\r\n\r\n";
			echo 'armor:'."\r\n";
			foreach ($armor as $value) 
			{
				echo  str_pad($value['name'], 20).' #'.$value['id'].' '.$value['itemStrength'].'/'.$value['strengthStart'];
				if ($value['equipped'] === 1)
				{
					echo ' (equipped in ' . $value['slot']. ')';
				}
				echo "\r\n";
			}
			echo "\r\n".'weapon:'."\r\n";
			foreach ($weapon as $value) 
			{
				echo str_pad($value['name'], 20).' #'.$value['id'].' '.$value['itemStrength'].'/'.$value['strengthStart'];
				if ($value['equipped'] === 1)
				{
					echo ' (equipped in ' . $value['slot']. ')';
				}
				echo "\r\n";
			}
			echo "\r\n".'ammunition:'."\r\n";
			foreach ($ammunition as $value) 
			{
				echo  str_pad($value['name'], 20).' #'.$value['id'].' ('.$value['quantity'];
				if ($value['equipped'] === 1)
				{
					echo '|equipped in ' . $value['slot'];
				}
				echo ')'."\r\n";
			}
			echo "\r\n".'food:'."\r\n";
			foreach ($food as $value) 
			{
				echo str_pad($value['name'], 20).' #'.$value['id'].' ('.$value['quantity'].')'."\r\n";
			}
		}
	}
	else
	{
		die ('Something went wrong (10):'."\r\n".print_r($result, true)."\r\n");
	}
}

function is_equipped()
{
	global $ammunition, $weapon, $ch, $domain, $version, $armor, $id_array, $companies;

	$equipped_weapon = 0;
	$equipped_ammo = 0;
	$equipped_ammo_slots = 0;
	$first_gun = FALSE;
	$second_gun = FALSE;
	$quick_1 = FALSE;
	$quick_2 = FALSE;
	$quick_3 = FALSE;
	$boots_inventory = FALSE;
	$pants_inventory = FALSE;
	$torso_inventory = FALSE;
	$head_inventory = FALSE;

	//update_storage();

	//check weapon slots
	foreach ($weapon as $value) 
	{
		if ($value['equipped'] === 1)
		{
			$equipped_weapon++;
			if ($value['slot'] === 'first_weapon_inventory')
			{
				$first_gun = TRUE;
			}
			else
			{
				$second_gun = TRUE;
			}
		}
	}

	//check ammo slots
	foreach ($ammunition as $value) 
	{
		if ($value['equipped'] === 1)
		{
			$equipped_ammo += $value['quantity'];
			$equipped_ammo_slots ++;
			if ($value['slot'] === 'quick_inventory_1')
			{
				$quick_1 = TRUE;
			}
			elseif ($value['slot'] === 'quick_inventory_2')
			{
				$quick_2 = TRUE;
			}
			else
			{
				$quick_3 = TRUE;
			}
		}
	}

	//check armor slots
	foreach ($armor as $value) 
	{
		if ($value['equipped'] === 1)
		{
			if ($value['slot'] === 'boots_inventory')
			{
				$boots_inventory = TRUE;
			}
			elseif ($value['slot'] === 'pants_inventory')
			{
				$pants_inventory = TRUE;
			}
			elseif ($value['slot'] === 'torso_inventory')
			{
				$torso_inventory = TRUE;
			}
			elseif ($value['slot'] === 'head_inventory')
			{
				$head_inventory = TRUE;
			}
		}
	}

	if (!$first_gun || !$second_gun)
	{
		get_military_companies_inventory();
		$count_need = 0;
		if (!$first_gun)
		{
			$count_need ++;
		}
		if (!$second_gun)
		{
			$count_need ++;
		}


		foreach ($companies as $company) 
		{
			if ($company['type_name'] === 'Military plant')
			{
				foreach ($company['items'] as $id => $value_c) 
				{
					$transfer_need = (($company['items'][$id] > 1) ? $count_need : 1);
					if ($count_need > 0 &&
						$id === $id_array['AK74_riffle'] &&
						$company['items'][$id] > 0 &&
						move_items_from_company($company['id'], $id_array['AK74_riffle'], $transfer_need))
					{
						$company['items'][$id] -= $transfer_need;
						update_storage();
						foreach ($weapon as $weapon_id => $value) 
						{
							if ($value['item_type'] === $id_array['AK74_riffle'] && !$first_gun && $weapon[$weapon_id]['equipped'] !== 1)
							{
								equip_item($value['id'], 'first_weapon_inventory');
								$first_gun = TRUE;
								$weapon[$weapon_id]['equipped'] === 1;
								$equipped_weapon ++;
								$count_need --;
								echo 'weapon #'.$value['id'].' equipped in first_weapon_inventory'."\r\n";
								continue;
							}
							if ($value['item_type'] === $id_array['AK74_riffle'] && !$second_gun && $weapon[$weapon_id]['equipped'] !== 1)
							{
								equip_item($value['id'], 'second_weapon_inventory');
								$second_gun = TRUE;
								$weapon[$weapon_id]['equipped'] === 1;
								$equipped_weapon ++;
								$count_need --;
								echo 'weapon #'.$value['id'].' equipped in second_weapon_inventory'."\r\n";
								continue;
							}
						}
						break;
					}
				}
			}
		}
		update_storage();

		// foreach ($weapon as $id => $value)
		// {
		// 	if ($value['equipped'] !== 1 && $equipped_weapon < 2)
		// 	{
		// 		if ($first_gun)
		// 		{
		// 			$slot = 'second_weapon_inventory';
		// 			$second_gun = TRUE;
		// 		}
		// 		else
		// 		{
		// 			$slot = 'first_weapon_inventory';
		// 			$first_gun = TRUE;
		// 		}
		// 		if (equip_item($value['id'], $slot))
		// 		{
		// 			echo 'weapon #'.$value['id'].' equipped in '.$slot."\r\n";
		// 			$equipped_weapon++;
		// 			$weapon[$id]['equipped'] = 1;
		// 			$weapon[$id]['slot'] = $slot;
		// 		}
		// 		else
		// 		{
		// 			die ('Something went wrong (9):'."\r\n".print_r($result, true)."\r\n");
		// 		}
		// 	}
		// }
		// echo "\r\n";
	}

	if (!$quick_1 || !$quick_2 || !$quick_3)
	{
		get_military_companies_inventory();
		$count_need = 0;
		if (!$quick_1)
		{
			$count_need += 250;
		}
		if (!$quick_2)
		{
			$count_need += 250;
		}
		if (!$quick_3)
		{
			$count_need += 250;
		}
		
		foreach ($companies as $company) 
		{
			if ($company['type_name'] === 'Military plant')
			{
				foreach ($company['items'] as $id => $value_c) 
				{
					//boots
					if ($id === $id_array['ammo_762mm'] &&
						$company['items'][$id] >= $count_need &&
						move_items_from_company($company['id'], $id_array['ammo_762mm'], $count_need))
					{
						$company['items'][$id] -= $count_need;
						update_storage();
						foreach ($ammunition as $ammo_id => $value) 
						{
							if ($value['item_type'] === $id_array['ammo_762mm'] && !$quick_1 && $ammunition[$ammo_id]['equipped'] !== 1)
							{
								equip_item($value['id'], 'quick_inventory_1');
								$quick_1 = TRUE;
								$ammunition[$ammo_id]['equipped'] === 1;
								$equipped_ammo += $value['quantity'];
								echo 'ammo #'.$value['id'].' equipped in quick_inventory_1'."\r\n";
								continue;
							}
							if ($value['item_type'] === $id_array['ammo_762mm'] && !$quick_2 && $ammunition[$ammo_id]['equipped'] !== 1)
							{
								equip_item($value['id'], 'quick_inventory_2');
								$quick_2 = TRUE;
								$ammunition[$ammo_id]['equipped'] === 1;
								$equipped_ammo += $value['quantity'];
								echo 'ammo #'.$value['id'].' equipped in quick_inventory_2'."\r\n";
								continue;
							}
							if ($value['item_type'] === $id_array['ammo_762mm'] && !$quick_3 && $ammunition[$ammo_id]['equipped'] !== 1)
							{
								equip_item($value['id'], 'quick_inventory_3');
								$quick_3 = TRUE;
								$ammunition[$ammo_id]['equipped'] === 1;
								$equipped_ammo += $value['quantity'];
								echo 'ammo #'.$value['id'].' equipped in quick_inventory_3'."\r\n";
								continue;
							}
						}
						break;
					}
				}
			}
		}
		update_storage();
		// foreach ($ammunition as $id => $value) 
		// {
		// 	if ($value['equipped'] !== 1 && $equipped_ammo_slots < 3)
		// 	{
		// 		if ($quick_1)
		// 		{
		// 			if ($quick_2)
		// 			{
		// 				$slot = 'quick_inventory_3';
		// 				$quick_3 = TRUE;
		// 			}
		// 			else
		// 			{
		// 				$slot = 'quick_inventory_2';
		// 				$quick_2 = TRUE;
		// 			}
		// 		}
		// 		else
		// 		{
		// 			$slot = 'quick_inventory_1';
		// 			$quick_1 = TRUE;
		// 		}
		// 		if (equip_item($value['id'], $slot))
		// 		{
		// 			echo 'ammunition #'.$value['id'].' equipped in '.$slot."\r\n";
		// 			$equipped_ammo_slots++;
		// 			$equipped_ammo += $value['quantity'];
		// 			$ammunition[$id]['equipped'] = 1;
		// 			$ammunition[$id]['slot'] = $slot;
		// 		}
		// 		else
		// 		{
		// 			die ('Something went wrong (8):'."\r\n".print_r($result, true)."\r\n");
		// 		}
		// 	}
		// }
		// echo "\r\n";
	}

	if (!$boots_inventory || !$pants_inventory || !$torso_inventory || !$head_inventory)
	{
		get_military_companies_inventory();
		//if empty find it on company, transfer to account and equip it
		foreach ($companies as $company) 
		{
			if ($company['type_name'] === 'Military plant')
			{
				foreach ($company['items'] as $id => $value) 
				{
					//boots
					if (!$boots_inventory && 
						$id === $id_array['membrane_boots'] &&
						$company['items'][$id] > 0 &&
						move_items_from_company($company['id'], $id_array['membrane_boots'], 1))
					{
						$company['items'][$id] --;
						update_storage();
						foreach ($armor as $value) 
						{
							if ($value['item_type'] === $id_array['membrane_boots'])
							{
								equip_item($value['id'], 'boots_inventory');
								$boots_inventory = TRUE;
								echo 'armor #'.$value['id'].' equipped in boots_inventory'."\r\n";
								break;
							}
						}
					}
					//helmet
					if (!$head_inventory && 
						$id === $id_array['helmet'] &&
						$company['items'][$id] > 0 &&
						move_items_from_company($company['id'], $id_array['helmet'], 1))
					{
						$company['items'][$id] --;
						update_storage();
						foreach ($armor as $value) 
						{
							if ($value['item_type'] === $id_array['helmet'])
							{
								equip_item($value['id'], 'head_inventory');
								$head_inventory = TRUE;
								echo 'armor #'.$value['id'].' equipped in head_inventory'."\r\n";
								break;
							}
						}
						if (!$head_inventory)
						{
							echo "helmet transfered but not equipped\r\n";
						}
					}
					//pants
					if (!$pants_inventory && 
						$id === $id_array['tactical_pants'] &&
						$company['items'][$id] > 0 &&
						move_items_from_company($company['id'], $id_array['tactical_pants'], 1))
					{
						$company['items'][$id] --;
						update_storage();
						foreach ($armor as $value) 
						{
							if ($value['item_type'] === $id_array['tactical_pants'])
							{
								equip_item($value['id'], 'pants_inventory');
								$pants_inventory = TRUE;
								echo 'armor #'.$value['id'].' equipped in pants_inventory'."\r\n";
								break;
							}
						}
					}
					//body armor
					if (!$torso_inventory && 
						$id === $id_array['OTV_body_armor'] &&
						$company['items'][$id] > 0 &&
						move_items_from_company($company['id'], $id_array['OTV_body_armor'], 1))
					{
						$company['items'][$id] --;
						update_storage();
						foreach ($armor as $value) 
						{
							if ($value['item_type'] === $id_array['OTV_body_armor'])
							{
								equip_item($value['id'], 'torso_inventory');
								$torso_inventory = TRUE;
								echo 'armor #'.$value['id'].' equipped in torso_inventory'."\r\n";
								break;
							}
						}
					}
				}
			}
		}
		update_storage();
	}

	if ($equipped_weapon < 2 || $equipped_ammo < 20)
	{
		echo 'you are not equipped'."\r\n\r\n";
		return FALSE;
	}
	else
	{
		return TRUE;
	}
}

function equip_item ($id, $slot)
{
	global $ch, $domain, $version;

	echo "equipping $id in $slot...\r\n";

	curl_setopt($ch, CURLOPT_URL, $domain . '/user_items/put_item_on.json' . $version);
	curl_setopt($ch, CURLOPT_POST, TRUE);
	curl_setopt($ch, CURLOPT_POSTFIELDS, 'data%5BUserItem%5D%5Bid%5D='.$id.'&data%5BUserItem%5D%5Bslot_id%5D='.$slot);

	$raw = curl_exec($ch);
	$result = json_decode($raw, true);
	if (isset($result['setFlash'][0]['class']) && $result['setFlash'][0]['class'] === 'flash_success')
	{
		return true;
	}
	else
	{
		var_dump(curl_error($ch));
		var_dump($raw);
		die ('Something went wrong (81):'."\r\n".print_r($result, true)."\r\n");
	}
}

function heal_injury()
{
	global $ch, $domain;
	curl_setopt($ch, CURLOPT_URL, $domain.'/hospitals/heal.json?os=android&v=1.05');
	//curl_setopt($ch, CURLOPT_POST, TRUE);
	curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
	
	$result = json_decode(curl_exec($ch), true);
	foreach ($result['User']['traumas'] as $key => $val)
	{
		curl_setopt($ch, CURLOPT_URL, $domain.'/hospitals/trauma_heal.json?os=android&v=1.05');
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "data%5BTrauma%5D%5Bid%5D=$key");
		
		$result = json_decode(curl_exec($ch), true);
		if (isset($result['setFlash'][0]['class']) && $result['setFlash'][0]['class'] === 'flash_success')
		{
			echo 'trauma #'.$key.' treated'."\r\n";
		}
		else
		{
			if ($result['setFlash'][0]['msg'] === 'Fail get lock for DoctorLicence.all')
			{
				sleep(2);
			}
			else
			{
				die ('Something went wrong (7):'."\r\n".print_r($result, true)."\r\n");
			}
		}
	}
	
	echo 'injuries treated'."\r\n";
}

function get_coach_license_id()
{
	global $ch, $CoachProgramId, $CoachProgramLicenseId, $userId, $domain;

	curl_setopt($ch, CURLOPT_URL, $domain."/sport_activities/train.json?os=android&v=1.05");
	curl_setopt($ch, CURLOPT_HTTPGET, TRUE);

	$result = json_decode(curl_exec($ch), true);

	if (isset($result['SportActivities']))
	{
		$coefficient = 1;
		$price = 1;
		foreach ($result['SportActivities']['programs'] as $value) 
		{
			if ($value['User']['id'] === $userId)
			{
				continue;
			}
			if ($value['CoachProgram']['coefficient'] > $coefficient && $value['CoachProgram']['price'] < 100 && $value['CoachProgram']['currency'] === 'vd')
			{
				$coefficient = $value['CoachProgram']['coefficient'];
				$price = $value['CoachProgram']['price'];
				$CoachProgramId = $value['CoachProgram']['id'];
				$CoachProgramLicenseId = $value['CoachProgram']['licence_id'];
			}
			if ($value['CoachProgram']['coefficient'] === $coefficient && $value['CoachProgram']['price'] < $price && $value['CoachProgram']['currency'] === 'vd')
			{
				$price = $value['CoachProgram']['price'];
				$CoachProgramId = $value['CoachProgram']['id'];
				$CoachProgramLicenseId = $value['CoachProgram']['licence_id'];
			}
		}
		//If we not found our license
		if ($CoachProgramId === 0 && $CoachProgramLicenseId === 0)
		{
			$CoachProgramId = $result['SportActivities']['programs'][0]['CoachProgram']['id'];
			$CoachProgramLicenseId = $result['SportActivities']['programs'][0]['CoachProgram']['licence_id'];
			echo 'user license NOT found'."\r\n";
		}
		else
		{
			echo $coefficient.' license for $'.$price.' found'."\r\n";
		}
	}
	else
	{
		die ('Something went wrong (6):'."\r\n".print_r($result, true)."\r\n");
	}
}

function heal()
{
	global $ch, $domain, $version;
	curl_setopt($ch, CURLOPT_URL, $domain.'/hospitals/heal_full_hp.json'.$version);
	curl_setopt($ch, CURLOPT_POST, TRUE);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "");
	
	$raw = curl_exec($ch);
	$result = json_decode($raw, true);
	if (isset($result['setFlash'][0]['class']) && $result['setFlash'][0]['class'] === 'flash_success')
	{
		echo 'health restored'."\r\n";
		update_info();
	}
	else
	{
		if ($result['setFlash'][0]['msg'] === 'Fail get lock for DoctorLicence.all' ||
			$result['setFlash'][0]['msg'] === 'NOT_ENOUGH_MONEY' ||
			$result['setFlash'][0]['msg'] === 'Something went wrong')
		{
			sleep(2);
			heal();
		}
		else
		{
			var_dump(curl_error($ch));
			var_dump($raw);
			die ('Something went wrong (5):'."\r\n".print_r($result, true)."\r\n");
		}
	}
}

function get_companies()
{
	global $ch, $domain, $version, $companies;

	curl_setopt($ch, CURLOPT_URL, $domain."/companies/user_companies_list/page:1.json".$version);
	curl_setopt($ch, CURLOPT_HTTPGET, TRUE);

	$result = json_decode(curl_exec($ch), true);
	if (isset($result['companies']))
	{
		foreach ($result['companies'] as $company)
		{
			$companies[$company['id']] = $company;
		}
	}
	else
	{
		die ('Something went wrong (6):'."\r\n".print_r($result, true)."\r\n");
	}

	curl_setopt($ch, CURLOPT_URL, $domain."/companies/user_companies_list/page:2.json".$version);
	curl_setopt($ch, CURLOPT_HTTPGET, TRUE);

	$result = json_decode(curl_exec($ch), true);
	if (isset($result['companies']))
	{
		foreach ($result['companies'] as $company)
		{
			$companies[$company['id']] = $company;
		}
	}
	else
	{
		die ('Something went wrong (6):'."\r\n".print_r($result, true)."\r\n");
	}

}

function update_companies_balance()
{
	global $ch, $domain, $version, $companies;

	foreach ($companies as $id => $company) 
	{
		curl_setopt($ch, CURLOPT_URL, $domain."/companies/info/".$id.".json".$version);
		curl_setopt($ch, CURLOPT_HTTPGET, TRUE);

		$result = json_decode(curl_exec($ch), true);

		if (isset($result['company']))
		{
			$companies[$id]['vd_balance'] = (float) $result['company']['vd_balance'];
		}
		else
		{
			die ('Something went wrong (61):'."\r\n".print_r($result, true)."\r\n");
		}
	}
}

function get_summ_vd_balance($companies)
{
	$summ = 0;
	foreach ($companies as $company) 
	{
		$summ += $company['vd_balance'];
	}
	return $summ;
}

function get_summ_foreign_employees($companies)
{
	$summ = 0;
	foreach ($companies as $company) 
	{
		$summ += $company['foreign_employees_limit'];
	}
	return $summ;
}

function transfer_vd_to_company($company_id, $vd_count)
{
	global $ch, $domain, $version;
	curl_setopt($ch, CURLOPT_URL, $domain . '/companies/add_funds_vd.json' . $version);
	curl_setopt($ch, CURLOPT_POST, TRUE);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "data%5BCompany%5D%5Bid%5D=".$company_id."&data%5BCompany%5D%5Bamount_add%5D=".$vd_count);

	$result = json_decode(curl_exec($ch), true);
	if (isset($result['setFlash'][0]['class']) && $result['setFlash'][0]['class'] === 'flash_success')
	{
		return true;
	}
	else
	{
		var_dump($result);
		return false;
	}
}

function get_military_companies_inventory()
{
	global $ch, $domain, $version, $companies, $id_array;
	foreach ($companies as $company) 
	{
		if ($company['type_name'] === 'Military plant')
		{
			$companies[$company['id']]['items'] = get_company_inventory($company['id']);
		}
	}
}

function get_company_inventory($id)
{
	global $ch, $domain, $version;

	curl_setopt($ch, CURLOPT_URL, $domain."/company_items/storage/".$id.".json".$version);
	curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
	$result = json_decode(curl_exec($ch), true);
	$return = array();
	if (isset($result['storage']))
	{
		foreach ($result['storage'] as $item) 
		{
			$return[$item['ItemType']['id']] = $item['CompanyItem']['quantity'];
		}
	}
	else
	{
		die ('Something went wrong (61):'."\r\n".print_r($result, true)."\r\n");
	}
	return $return;
}

function move_items_from_company_to_other_company($id_from, $id_to, $item_id, $count)
{
	global $ch, $domain, $version, $free_slots;

	echo $id_from.' == '.$item_id.' ('.$count.') ==> '.$id_to."\r\n";

	if ($free_slots === 0)
	{
		echo "No free slots \r\n";
		return false;
	}

	if ($count < 1)
	{
		echo "incorrect count \r\n";
		return false;
	}

	if ($id_from === $id_to)
	{
		echo "Companies id are equal\r\n";
		return false;
	}

	while ($count > 0) 
	{
		if ($count > $free_slots * 250)
		{
			$transfer_count = $free_slots * 250;
		}
		else
		{
			$transfer_count = $count;
		}

		echo $id_from.' == '.$item_id.' ('.$transfer_count.') ==> user'."\r\n";

		curl_setopt($ch, CURLOPT_URL, $domain . '/company_items/move_items_to_user/' . 
																$id_from . '/' . 
																$item_id . '/' . 
																$transfer_count . '.json' . $version);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "");
		$result = json_decode(curl_exec($ch), true);
		if (!isset($result['setFlash'][0]['class']) || $result['setFlash'][0]['class'] !== 'flash_success')
		{
			var_dump($result);
			return false;
		}

		echo 'user == '.$item_id.' ('.$transfer_count.') ==> '.$id_to."\r\n";

		curl_setopt($ch, CURLOPT_URL, $domain . '/user_items/move_items_to_company/' . 
																$id_to . '/' . 
																$item_id . '/' . 
																$transfer_count . '.json' . $version);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "");
		$result = json_decode(curl_exec($ch), true);
		if (!isset($result['setFlash'][0]['class']) || $result['setFlash'][0]['class'] !== 'flash_success')
		{
			var_dump($result);
			return false;
		}
		$count -= $transfer_count;
		echo "\r\n";
	}
	echo "\r\n";
	return true;
}

function move_items_from_to($id_item, $from, $to)
{
	global $ch, $domain, $version, $companies;

	$items_count = 0;
	$items_to_count = 0;

	foreach ($from as $id) 
	{
		$companies[$id]['items'] = get_company_inventory($id);
		if (isset($companies[$id]['items'][$id_item]))
		{
			$items_count += $companies[$id]['items'][$id_item];
		}
	}
	if ($items_count === 0)
	{
		echo "nothing to transfer \r\n";
		return false;
	}
	// foreach ($to as $id => $part) 
	// {
	// 	$companies[$id]['items'] = get_company_inventory($id);
	// 	if (isset($companies[$id]['items'][$id_item]))
	// 	{
	// 		$items_to_count += $companies[$id]['items'][$id_item];
	// 	}
	// }

	$items_summ = $items_count;// + $items_to_count;

	foreach ($to as $id => $part) 
	{
		$count_need = floor($items_summ * $part);
		// if (isset($companies[$id]['items'][$id_item]))
		// {
		// 	if ($companies[$id]['items'][$id_item] > $count_need)
		// 	{
		// 		$count_need = 0;
		// 	}
		// 	else
		// 	{
		// 		$count_need -= $companies[$id]['items'][$id_item];
		// 	}
		// }
		foreach ($from as $id_from) 
		{
			if (isset($companies[$id_from]['items'][$id_item]) && $companies[$id_from]['items'][$id_item] > 0)
			{
				if ($companies[$id_from]['items'][$id_item] >= $count_need)
				{
					$companies[$id_from]['items'][$id_item] -= $count_need;
					move_items_from_company_to_other_company($id_from, $id, $id_item, $count_need);
					break;
				}
				else
				{
					$transfer = $companies[$id_from]['items'][$id_item];
					$companies[$id_from]['items'][$id_item] = 0;
					$count_need -= $transfer;
					move_items_from_company_to_other_company($id_from, $id, $id_item, $transfer);
				}
			}
		}
	}
}

function move_items_from_company($id_from, $item_id, $count)
{
	global $ch, $domain, $version, $free_slots;

	echo "transfering $item_id ($count) from $id_from...\r\n";

	if ($free_slots === 0)
	{
		echo "No free slots \r\n";
		return false;
	}

	if ($count < 1)
	{
		echo "Uncorrect count \r\n";
		return false;
	}

	curl_setopt($ch, CURLOPT_URL, $domain . '/company_items/move_items_to_user/' . 
																$id_from . '/' . 
																$item_id . '/' . 
																$count . '.json' . $version);
	curl_setopt($ch, CURLOPT_POST, TRUE);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "");
	$result = json_decode(curl_exec($ch), true);
	if (!isset($result['setFlash'][0]['class']) || $result['setFlash'][0]['class'] !== 'flash_success')
	{
		var_dump($result);
		return false;
	}
	return true;
}

function eat()
{
	global $ch, $domain, $version, $food, $eatEffects, $delta_recovery_energy, $id_array;

	//update_storage();
	update_info();

	$grain = FALSE;
	$fish_pie = FALSE;
	$meat_bread = FALSE;
	$fishers_pizza = FALSE;
	$stew = FALSE;
	$milk = FALSE;
	$meat = FALSE;
	$vegetables = FALSE;
	$fish = FALSE;

	if ($delta_recovery_energy > 90.3)
	{
		echo 'player is hungry'."\r\n";
		foreach ($eatEffects as $effect) 
		{
			if ($effect['item'][0]['name'] === 'Grain')
			{
				$grain = TRUE;
			}
			if ($effect['item'][0]['name'] === 'Fish pie')
			{
				$fish_pie = TRUE;
			}
			if ($effect['item'][0]['name'] === 'Meat bread')
			{
				$meat_bread = TRUE;
			}
			if ($effect['item'][0]['name'] === 'Fisher\'s pizza')
			{
				$fishers_pizza = TRUE;
			}
			if ($effect['item'][0]['name'] === 'Stew')
			{
				$stew = TRUE;
			}
			if ($effect['item'][0]['name'] === 'Fish')
			{
				$fish = TRUE;
			}
			if ($effect['item'][0]['name'] === 'Vegetables')
			{
				$vegetables = TRUE;
			}
			if ($effect['item'][0]['name'] === 'Meat')
			{
				$meat = TRUE;
			}
			if ($effect['item'][0]['name'] === 'Milk')
			{
				$milk = TRUE;
			}


		}
		foreach ($food as $item) 
		{
			if (!$grain && $item['item_type'] === $id_array['grain'])
			{
				echo 'eating Grain...'."\r\n";
				eat_item ($item['id']);
				$grain = TRUE;
			}
			if (!$fish_pie && $item['item_type'] === $id_array['fish_pie'])
			{
				echo 'eating Fish pie...'."\r\n";
				eat_item ($item['id']);
				$fish_pie = TRUE;
			}
			if (!$meat_bread && $item['item_type'] === $id_array['meat_bread'])
			{
				echo 'eating Meat bread...'."\r\n";
				eat_item ($item['id']);
				$meat_bread = TRUE;
			}
			if (!$fishers_pizza && $item['item_type'] === $id_array['fishers_pizza'])
			{
				echo 'eating Fisher\'s pizza...'."\r\n";
				eat_item ($item['id']);
				$fishers_pizza = TRUE;
			}
			if (!$stew && $item['item_type'] === $id_array['stew'])
			{
				echo 'eating Stew...'."\r\n";
				eat_item ($item['id']);
				$stew = TRUE;
			}
			if (!$milk && $item['item_type'] === $id_array['milk'])
			{
				echo 'eating Milk...'."\r\n";
				eat_item ($item['id']);
				$milk = TRUE;
			}
			if (!$meat && $item['item_type'] === $id_array['meat'])
			{
				echo 'eating Meat...'."\r\n";
				eat_item ($item['id']);
				$meat = TRUE;
			}
			if (!$fish && $item['item_type'] === $id_array['fish'])
			{
				echo 'eating Fish...'."\r\n";
				eat_item ($item['id']);
				$fish = TRUE;
			}
			if (!$vegetables && $item['item_type'] === $id_array['vegetables'])
			{
				echo 'eating Vegetables...'."\r\n";
				eat_item ($item['id']);
				$vegetables = TRUE;
			}

		}
	}
	return true;
}

function eat_item ($item_id)
{
	global $ch, $domain, $version;

	curl_setopt($ch, CURLOPT_URL, $domain . '/user_items/eat_user_item.json' . $version);
	curl_setopt($ch, CURLOPT_POST, TRUE);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "data%5BUserItem%5D%5Bitem_id%5D=".$item_id);

	$raw = curl_exec($ch);
	$result = json_decode($raw, true);
	if (isset($result['setFlash'][0]['class']) && $result['setFlash'][0]['class'] === 'flash_success')
	{
		return true;
	}
	else
	{
		var_dump(curl_error($ch));
		var_dump($raw);
		die ('Something went wrong (91):'."\r\n".print_r($result, true)."\r\n");
	}
}

function print_news()
{
	global $ch, $domain, $version, $companies;

	curl_setopt($ch, CURLOPT_URL, $domain."/newspaper_posts/latest.json".$version);
	curl_setopt($ch, CURLOPT_HTTPGET, TRUE);

	$result = json_decode(curl_exec($ch), true);
	$articles = array();
	foreach ($result['newspaperPosts'] as $id => $post) 
	{
		 // var_dump($post);
		 // die();
		/*$articles[] = */
		write_article_to_db(array('id' 		=> $post['NewspaperPost']['id'],
							'title'			=> $post['NewspaperPost']['title'],
							'body' 			=> $post['NewspaperPost']['body'],
							'created'		=> $post['NewspaperPost']['created'],
							'updated'		=> $post['NewspaperPost']['updated'],
							'likes'			=> $post['NewspaperPost']['likes'],
							'moderated'		=> $post['NewspaperPost']['moderated'],
							'comments_count'=> $post['NewspaperPost']['newspaper_post_comment_count'],
							'comments'		=> $post['NewspaperPostComment'],
							'newspaper'		=> array('id'	=> $post['NewspaperPost']['newspaper_id'],
													 'name'	=> $post['Newspaper']['name'],
													 'city'	=> $post['Newspaper']['city_id'],
													 'subs' => $post['Newspaper']['newspaper_subscription_count'])
							));
	}
	// ob_start();
	// var_dump($articles);
	// $file = ob_get_clean();
	// file_put_contents('news.txt', $file);
}

function write_article_to_db($article)
{
	global $mysqli;

	$newspaper = $article['Newspaper'];
	$result = $mysqli->query("SELECT id FROM newspapers 
		WHERE id = '".$mysqli->real_escape_string($newspaper['id'])."'");

	if ($result->num_rows === 1)
	{
		$mysqli->query("UPDATE `newspapers` SET name = '".$mysqli->real_escape_string($newspaper['name'])."', subs = '".$mysqli->real_escape_string($newspaper['newspaper_subscription_count'])."' 
			WHERE id = '".$mysqli->real_escape_string($newspaper['id'])."'");
	}
	else
	{
		echo 'add '.$newspaper['id']."\r\n";
		$mysqli->query("INSERT INTO `newspapers`(`id`, `user_id`, `name`, `city`, `subs`) 
			VALUES ('".$mysqli->real_escape_string($newspaper['id'])."', '".$mysqli->real_escape_string($newspaper['user_id'])."', '".$mysqli->real_escape_string($newspaper['name'])."', '".$mysqli->real_escape_string($newspaper['city_id'])."', '".$mysqli->real_escape_string($newspaper['newspaper_subscription_count'])."')");
	}

	$autor = $newspaper['User'];
	$mysqli->query("INSERT INTO `users`(`id`, `username`, `email`, `device_platform`) 
			VALUES ('".$mysqli->real_escape_string($autor['id'])."', '".$mysqli->real_escape_string($autor['username'])."', '".$mysqli->real_escape_string($autor['email'])."', '".$mysqli->real_escape_string($autor['device_platform'])."')");

	$post = $article['NewspaperPost'];

	$result = $mysqli->query("SELECT id FROM articles 
		WHERE id = '".$mysqli->real_escape_string($post['id'])."'");

	if ($result->num_rows === 1)
	{
			echo 'update '.$post['id']."\r\n";
			$mysqli->query("UPDATE `articles` SET title = '".$mysqli->real_escape_string($post['title'])."', body = '".$mysqli->real_escape_string($post['parsed_body'])."', likes = '".$mysqli->real_escape_string($post['likes'])."', comments_count = '".$mysqli->real_escape_string($post['newspaper_post_comment_count'])."' 
			WHERE id = '".$mysqli->real_escape_string($post['id'])."'");
	}
	else
	{
		echo 'add '.$post['id']."\r\n";
		$mysqli->query("INSERT INTO `articles` (`id`, `newspaper_id`, `title`, `body`, `created`, `likes`, `comments_count`) 
			VALUES ('".$mysqli->real_escape_string($post['id'])."', '".$mysqli->real_escape_string($post['newspaper_id'])."', '".$mysqli->real_escape_string($post['title'])."', '".$mysqli->real_escape_string($post['parsed_body'])."', '".$mysqli->real_escape_string($post['created'])."', '".$mysqli->real_escape_string($post['likes'])."',  '".$mysqli->real_escape_string($post['newspaper_post_comment_count'])."')");
	}

	$comments = $article['NewspaperPostComment'];
	foreach ($comments as $key => $comment) 
	{
		$mysqli->query("INSERT INTO `comments`(`id`, `article_id`, `user_id`, `body`, `created`) 
			VALUES ('".$mysqli->real_escape_string($comment['id'])."', '".$mysqli->real_escape_string($comment['newspaper_post_id'])."', '".$mysqli->real_escape_string($comment['user_id'])."', '".$mysqli->real_escape_string($comment['body'])."', '".$mysqli->real_escape_string($comment['created'])."')");
		$user = $comment['User'];
		$mysqli->query("INSERT INTO `users`(`id`, `username`, `email`, `device_platform`) 
			VALUES ('".$mysqli->real_escape_string($user['id'])."', '".$mysqli->real_escape_string($user['username'])."', '".$mysqli->real_escape_string($user['email'])."', '".$mysqli->real_escape_string($user['device_platform'])."')");
	}
}

function get_article($id)
{
	global $ch, $domain, $version;

	curl_setopt($ch, CURLOPT_URL, $domain."/newspaper_posts/view_post/".$id.".json".$version);
	curl_setopt($ch, CURLOPT_HTTPGET, TRUE);

	$result = json_decode(curl_exec($ch), true);
	return $result;
}

function initialize_stats()
{
	global $mysqli, $last_band, $last_gang, $last_rank;

	$mysqli->query("INSERT INTO `gangs_stats` (`band_id`, `gang_id`, `rank_id`)
					SELECT * FROM (SELECT '$last_band' as one, '$last_gang' as two, '$last_rank' as three) AS tmp
					WHERE NOT EXISTS (
						SELECT win 
						FROM `gangs_stats`
						WHERE `band_id` = '$last_band' AND `gang_id` = '$last_gang' AND `rank_id` = '$last_rank'
					) LIMIT 1;");
}

function write_stats($status, $damage, $all_damage, $next_rank, $money = 0)
{
	global $mysqli, $last_band, $last_gang, $last_rank;
	if ($status === 'lose')
	{
		$mysqli->query("UPDATE `gangs_stats` 
						SET `lose` = `lose` + 1, `damage` = `damage` + $damage
						WHERE `band_id` = '$last_band' AND `gang_id` = '$last_gang' AND `rank_id` = '$last_rank'");
	}
	if ($status === 'win')
	{
		$mysqli->query("UPDATE `gangs_stats` 
						SET `win` = `win` + 1, `damage` = `damage` + $damage, `money` = `money` + $money
						WHERE `band_id` = '$last_band' AND `gang_id` = '$last_gang' AND `rank_id` = '$last_rank'");
	}
	$result = $mysqli->query("SELECT `win`, `lose`, `damage`, `money`
							  FROM `gangs_stats`
							  WHERE `band_id` = '$last_band' AND `gang_id` = '$last_gang' AND `rank_id` = '$last_rank'");
	$row = $result->fetch_row();
	$percent = round(($row[0]*100)/($row[0]+$row[1]));
	echo 'winning percentage: '.$percent.' ('.$row[0].'/'.$row[1].')'."\r\n";
	echo 'damage: '.$damage.' ('.round(($row[2]/($row[0]+$row[1]))).', '.($next_rank-$all_damage).', '.round(($next_rank-$all_damage)/($row[2]/($row[0]+$row[1]))).')'."\r\n";
	echo 'money: '.$money.' ('.round($row[3]/$row[0]).' | '.round($row[3]/($row[0]+$row[1])).', '.round(round($row[3]/($row[0]+$row[1]))/6584*30, 2).") \r\n";
}